﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBoard : MonoBehaviour
{
    [SerializeField]
    private GameObject blockPrefab;

    [SerializeField]
    private Sprite[] blockSprite;

    private struct Block
    {
        public int x;
        public int y;
        public GameObject obj;

        public Block (int x, int y, GameObject obj)
        {
            this.x = x;
            this.y = y;
            this.obj = obj;
        }
    }

    private Block[] figure = new Block[4]
    {
        new Block(),
        new Block(),
        new Block(),
        new Block()
    };

    private int W = 10;
    private int H = 20;

    private Block[,] group;

    private int[,] shapes = new int[,]
    {
        {1, 3, 5, 7},
        {2, 4, 5, 7},
        {3, 4, 5, 6},
        {3, 4, 5, 7},
        {2, 3, 5, 7},
        {3, 5, 6, 7},
        {2, 3, 4, 5}
    };

    private float moveTime = 0f;
    private float moveSpeed = 0.06f;
    private float time = 0f;
    private float dropSpeed = 0.4f;
    // Start is called before the first frame update
    void Start()
    {
        group = new Block[W, H];
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKey(KeyCode.LeftArrow) )
        {
            HoldAndMove(-1, 0);
        }
        else if ( Input.GetKey(KeyCode.RightArrow) )
        {
            HoldAndMove(1, 0);
        }
        else if ( Input.GetKeyDown(KeyCode.UpArrow) )
        {
            Rotate();
        }
        else if( Input.GetKeyDown(KeyCode.DownArrow) )
        {
            dropSpeed = 0.05f;
        }
        time += Time.deltaTime;
        if(time > dropSpeed)
        {
            if( !Move(0, -1) )
            {
                for(int i = 0; i < 4; i++)
                {
                     group[figure[i].x, -figure[i].y] = figure[i];
                }
                Generate();
                Clean();
            }
            time = 0;
        }
    }
 
    private void Generate()
    {
        dropSpeed = 0.4f;
         int n = Random.Range( 0, shapes.GetLength(0) ); 
         for(int i = 0; i < 4; i++)
         {
             figure[i].x = shapes[n, i] % 2;
             figure[i].y = - shapes[n, i] / 2;
         }

         Sprite sprite = blockSprite[ Random.Range( 0, blockSprite.Length ) ];
         for(int i = 0; i < 4; i++)
         {
            figure[i].obj = Instantiate(blockPrefab, new Vector2(figure[i].x, figure[i].y), Quaternion.identity);
            SpriteRenderer sr = figure[i].obj.GetComponent<SpriteRenderer>();
            sr.sprite = sprite;
         }
         Move(4, 0);
    }
    
    private void HoldAndMove(int dx, int dy)
    {
        moveTime += Time.deltaTime;
        if (moveTime > moveSpeed)
        {
            Move(dx, dy);
            moveTime = 0;
        }
    }
    private bool Move(int dx, int dy)
    {
        Block[] origin = figure.Clone() as Block[];
        for(int i = 0; i < 4; i++)
        {
            figure[i].x += dx;
            figure[i].y += dy;
        }
        return CheckAndSet(origin);
    }

    private void Rotate()
    {
        Block[] origin = figure.Clone() as Block[];
        Block ind = figure[1];
        for(int i = 0; i < 4; i++)
        {
            int x = figure[i].y - ind.y;
            int y = figure[i].x - ind.x;
            
            figure[i].x = ind.x - x;
            figure[i].y = ind.y + y;
        }
        CheckAndSet(origin);
    }

    private bool CheckAndSet(Block[] ori)
    {
        bool set = true;
        for(int i = 0; i < 4; i++)
        {
            if( figure[i].x < 0 || figure[i].x > 9 || figure[i].y >= W || figure[i].y <= -H )
            {
                set = false;
            }
            else if( group[figure[i].x, -figure[i].y].obj != null)
            {
                set = false;
            }
        }
        if(set)
        {
            for(int i = 0; i < 4; i++)
            {
                figure[i].obj.transform.position = new Vector2(figure[i].x, figure[i].y);
            }
        }
        else
        {
            figure = ori;
        }
        
        return set;
    }

    private void Clean()
    {
        List<Block> blockToClear = new List<Block>();
        int k = H - 1;
        int dy = 0;

        for(int i = H - 1; i > 0; i--)
        {
            blockToClear.Clear();
            int cnt = 0;
            for(int j = 0; j < W; j++)
            {
                if( group[j, i].obj != null)
                {
                    cnt++;
                }
                group[j, i].y += dy;
                blockToClear.Add( group[j, i] );
                group[j, k] = group[j, i];
            } 
            if( cnt < W )
            {
                k--;
            }
            else
            {
                dy--; 
                for(int n = 0; n < blockToClear.Count; n++)
                {
                    Destroy(blockToClear[n].obj);
                }
            }

            for(int j = 0; j < W; j++)
            {
                if( group[j, i].obj != null )
                {
                    group[j, i].obj.transform.position = new Vector2( group[j, i].x, group[j, i].y );
                }
            }
        }
    }
}
